A RimWorld mod to control and inspect various convoluted mechanics regarding weather.

**Recommended Mod Introduction**
https://youtu.be/ZO5TJxx3x6k

**Features**
- Configure a biomes weathers and their commonality within that biome
- Configure the duration range for weather
- Wind sock building to display current wind speed
- Weather station building to display various weather information

**How Commonality works**
In the game the WeatherManager is responsible for determining the next weather. It does so by taking all the weathers available for the current biome of the map and summing up all the commonalities of the weathers to select a random one by weight. Meaning a weather with a high commonality such as Clear has a higher chance of being picked. This mod lets you play with those values to change a given biomes commonalities and change the cocktail of weather frequencies.

**Buildings**
The buildings present some mildly interesting internal values of the weather system.
To build weather stations you must first complete the matching research (after Microelectronics).
The buildings have little "purpose" and are mostly neat flavor and can be used as functional decoration.

**Safety**
It is safe to add or remove new biomes or weathers while WeatherControl is installed, it will automatically pick up on any changes and present options on how to resolve issues.
However, installing new biomes mid-playthrough WILL break your game. This is base-game behaviour and will not be fixed by this mod.

**Afterword**
This mod is mostly a convenience tool, everything regarding commonalities in this mod does can be done via PatchOperation modding - but it offers some visual aid to understand the values that you tweak.