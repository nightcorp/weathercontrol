﻿using NightmareCore;
using NightmareCore.Utilities;
using RimWorld;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class TemperatureControlTab : SmartModSettingsTab
    {
        private float contentHeight = 9999;
        private Vector2 scrollPosition;
        TemperatureControlManager manager;

        protected override string TabTranslationKey => "NCWC_Settings_TemperatureControlTab";

        public TemperatureControlTab() : base() { }

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            manager = WeatherControlMod.Settings.temperatureManager;
        }

        public override void Notify_TabClosed()
        {
            base.Notify_TabClosed();
        }

        public override void DrawContent(Rect inRect)
        {
            UIUtility.MakeAndBeginScrollView(inRect, contentHeight, ref scrollPosition, out Listing_Standard list);
            list.CheckboxLabeled("NCWC_Settings_TemperatureControlsEnabled".Translate(), ref manager.isEnabled);
            
            if (!manager.isEnabled )
            {
                UIUtility.EndScrollView(list, out contentHeight);
                return;
            }
            list.Label("NCWC_Settings_TemperatureControl_Tip".Translate());
            DrawTimeOfDayTemperatureControls(list);
            DrawSeasonTemperatureControls(list);

            UIUtility.EndScrollView(list, out contentHeight);
        }

        const float maxTemperatureOffset = 40f;
        const float minTemperatureOffset = -40f;
        private static Func<float, string> temperatureValuePresenter = (float temperature) => $"{(temperature >= 0 ? "+" : "")}{temperature.ToStringTemperature()}";
        static UIUtility.DraggableMatrixLineParameters timeParameters = new UIUtility.DraggableMatrixLineParameters()
        {
            yMin = minTemperatureOffset,
            yMax = maxTemperatureOffset,
            showTooltip = true,
            xAxisTranslationKey = "NCWC_Settings_TemperatureControl_TimeOfDay",
            yAxisTranslationKey = "NCWC_Settings_TemperatureControl_TemperatureOffset",
            showLabels = true,
            xValuePresenter = (float f) => $"{(int)f}{"LetterHour".Translate()}",
            yValuePresenter = temperatureValuePresenter,
            showGrid = true,
            yGridSteps = 20,
            xGridSteps = 48,
            roundYValue = true,
            roundYValueTo = 2f,
            lineWidth = 2,
            lineColor = Color.white,
        };
        private void DrawTimeOfDayTemperatureControls(Listing_Standard list)
        {
            list.HeaderLabel("NCWC_Settings_TemperatureControl_TimeOfDayModifiers".Translate());
            Rect gridRect = list.GetRect(Text.LineHeight * 10);
            Rect resetRect = gridRect.RightPartPixels(Text.LineHeight);
            gridRect = gridRect.ContractedBy(marginRight: resetRect.width);
            Color? resetColor = Mouse.IsOver(resetRect) ? (Color?)Color.blue : null;
            UIUtility.DraggableMatrixLine(gridRect, manager.timeToTemperatureOffsets, timeParameters);
            UIUtility.TextureInCenter(resetRect, UITextures.ResetTexture, out _, resetRect.width, resetRect.width, color: resetColor);
            if (Widgets.ButtonInvisible(resetRect))
            {
                manager.SetOrResetTimeOffsets();
            }
            list.Gap();
        }

        UIUtility.DraggableMatrixLineParameters seasonParameters = new UIUtility.DraggableMatrixLineParameters()
        {
            yMin = minTemperatureOffset,
            yMax = maxTemperatureOffset,
            showTooltip = true,
            xAxisTranslationKey = "NCWC_Settings_TemperatureControl_DayOfSeason",
            yAxisTranslationKey = "NCWC_Settings_TemperatureControl_TemperatureOffset",
            showLabels = true,
            xValuePresenter = (float f) => $"{"Day".Translate()} {f}",
            yValuePresenter = temperatureValuePresenter,
            showGrid = true,
            yGridSteps = 20,
            xGridSteps = 30,
            roundYValue = true,
            roundYValueTo = 2f,
            lineWidth = 2,
            lineColor = Color.white,
        };

        private void DrawSeasonTemperatureControls(Listing_Standard list)
        {
            list.HeaderLabel("NCWC_Settings_TemperatureControl_SeasonModifiers".Translate());
            
            foreach (Season season in manager.seasonOffsets.Keys.ToList())
            {
                //Dictionary<float, float> dayToOffset = manager.seasonOffsets[season].dayToOffsets;
                SimpleCurve curve = manager.seasonOffsets[season].dayToOffsets;
                list.HeaderLabel(season.LabelCap(), false);
                Rect gridRect = list.GetRect(Text.LineHeight * 10);
                Rect resetRect = gridRect.RightPartPixels(Text.LineHeight);
                gridRect = gridRect.ContractedBy(marginRight: resetRect.width);
                Color? resetColor = Mouse.IsOver(resetRect) ? (Color?)Color.blue : null;
                UIUtility.DraggableMatrixLine(gridRect, curve, seasonParameters);
                UIUtility.TextureInCenter(resetRect, UITextures.ResetTexture, out _, resetRect.width, resetRect.width, color: resetColor);
                if (Widgets.ButtonInvisible(resetRect))
                {
                    manager.ResetSeasonoffsets(season);
                }
                list.Gap();
            }
        }

    }
    
}
