﻿using NightmareCore;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class WeatherEventControlTab : SmartModSettingsTab
    {
        public WeatherEventControlTab() : base() { }

        float contentHeight = 9999f;
        Vector2 scrollPosition;
        static WeatherEventOverrideManager manager;

        protected override string TabTranslationKey => "NCWC_Settings_WeatherEventControlTab";

        public override void DefsLoaded()
        {
            base.DefsLoaded();
            manager = WeatherControlMod.Settings.weatherEventOverrides;
        }

        public override void Notify_TabClosed()
        {
            manager.ApplyDefModifications();
        }

        public override void DrawContent(Rect inRect)
        {
            UIUtility.MakeAndBeginScrollView(inRect, contentHeight, ref scrollPosition, out Listing_Standard list);
            list.HeaderLabel("NCWC_Settings_WeatherEventIntervals".Translate(), false);
            for(int i = 0; i < DefDatabase<WeatherDef>.AllDefsListForReading.Count; i++)
            {
                if(i != 0)
                {
                    list.GapLine();
                }
                WeatherDef weather = DefDatabase<WeatherDef>.AllDefsListForReading[i];
                Rect rowRect = list.GetRect(Text.LineHeight);
                Rect resetRect = rowRect.RightPartPixels(rowRect.height);
                rowRect.xMax -= resetRect.width;
                Widgets.Label(rowRect, weather.LabelCap);
                TooltipHandler.TipRegion(resetRect, "NCWC_Settings_ResetWeatherEvents".Translate());
                if(Widgets.ButtonImage(resetRect, UITextures.ResetTexture))
                {
                    WeatherControlMod.Settings.weatherEventOverrides.Reset(weather);
                }
                list.IndentAndReduceWidth();
                List<WeatherEventMaker> eventMakers = WeatherControlMod.Settings.weatherEventOverrides.Get(weather);
                for(int j = 0; j < eventMakers.Count; j++)    // for loop to prevent CollectionModifiedException
                {
                    WeatherEventMaker eventMaker = eventMakers[j];
                    DrawEntry(list, weather, eventMaker);
                }
                list.OutdentAndIncreaseWidth();
                Rect addRect = list.GetRect(Text.LineHeight);
                addRect = addRect.LeftPartPixels(addRect.height);
                TooltipHandler.TipRegion(addRect, "NCWC_Settings_AddWeatherEvent".Translate());
                UIUtility.TextureInCenter(addRect, TexButton.Plus, out _);
                if(Widgets.ButtonInvisible(addRect))
                {
                    OpenAddWeatherEventWindow(weather);
                }
            }   

            UIUtility.EndScrollView(list, out contentHeight);
        }

        private void DrawEntry(Listing_Standard list, WeatherDef weather, WeatherEventMaker eventMaker)
        {
            string label = $"{ExtractPresentableLabel(eventMaker.eventClass)}: {Mathf.RoundToInt(eventMaker.averageInterval).ToStringTicksToPeriodVerbose()}";
            Rect rowRect = list.GetRect(Text.LineHeight);
            Rect removeButtonRect = rowRect.RightPartPixels(rowRect.height);
            rowRect.xMax -= removeButtonRect.width;
            Widgets.Label(rowRect, label);
            UIUtility.TextureInCenter(removeButtonRect, TexButton.Minus, out _);
            if(Widgets.ButtonInvisible(removeButtonRect))
            {
                WeatherControlMod.Settings.weatherEventOverrides.Remove(weather, eventMaker);
            }

            eventMaker.averageInterval = list.Slider(eventMaker.averageInterval, Common.MinWeatherDuration, Common.MaxWeatherEventDuration);
        }

        const float defaultAverageInterval = 1200;
        private void OpenAddWeatherEventWindow(WeatherDef weather)
        {
            if(cachedWeatherEventTypesWithLabel == null)
            {
                cachedWeatherEventTypesWithLabel = new Dictionary<Type, string>();
                RecacheWeatherEventTypes();
            }
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            foreach(KeyValuePair<Type, string> kvp in cachedWeatherEventTypesWithLabel)
            {
                Type type = kvp.Key;
                string label = kvp.Value;
                options.Add(new FloatMenuOption(label, () =>
                {
                    WeatherControlMod.Settings.weatherEventOverrides.Add(weather, type, defaultAverageInterval);
                }));
            }
            if(options.NullOrEmpty())
            {
                options.Add(new FloatMenuOption("NCWC_Settings_NoWeatherEventsAvailable".Translate(), null));
            }
            Find.WindowStack.Add(new FloatMenu(options));
        }

        static Dictionary<Type, string> cachedWeatherEventTypesWithLabel;

        public void RecacheWeatherEventTypes()
        {
            cachedWeatherEventTypesWithLabel.Clear();
            foreach(Type type in GenTypes.AllTypes)
            {
                if(type.IsAbstract)
                {
                    continue;
                }
                if(type.IsSubclassOf(typeof(WeatherEvent)))
                {
                    cachedWeatherEventTypesWithLabel.Add(type, ExtractPresentableLabel(type));
                }
            }
            //Log.Message($"Finished caching event types: {string.Join(", ", cachedWeatherEventTypes)}");
        }

        private static string ExtractPresentableLabel(Type type)
        {
            string label = type.ToString();
            label = label.Substring(label.LastIndexOf(".") + 1);
            label = label.Replace("WeatherEvent", "");
            label = label.Replace("_", " ");
            label = label.Trim();
            label = Regex.Replace(label, "([a-z]{1})([A-Z]{1})", "${1} ${2}"); // transforms stuff like "LightingStrikeDelayed" to "Lighting Strike Delayed"
            return label;
        }
    }
}
