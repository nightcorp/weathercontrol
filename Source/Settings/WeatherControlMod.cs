﻿using NightmareCore.Settings;
using NightmareCore.Utilities;
using RimWorld;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class WeatherControlMod : SmartMod
    {
        public static WeatherControlSettings Settings;
        public WeatherControlMod(ModContentPack content) : base(content)
        {
            Settings = GetSettings<WeatherControlSettings>();
            InternalSettings = Settings;
        }

        public override string SettingsCategory()
        {
            return "NCWC_SettingsCategory".Translate();
        }
    }

    public class WeatherControlSettings : SmartModSettings
    {
        TabDrawMode tabDrawMode = TabDrawMode.Default;
        public BiomeWeatherOverrideManager biomeWeatherOverrides = new BiomeWeatherOverrideManager();
        public WeatherDurationOverrideManager weatherDurationOverrides = new WeatherDurationOverrideManager();
        public WeatherEventOverrideManager weatherEventOverrides = new WeatherEventOverrideManager();
        public TemperatureControlManager temperatureManager = new TemperatureControlManager();

        BiomeControlTab biomeControlTab;
        WeatherDurationControlTab weatherDurationControlTab;
        // sadly no mods ever make use of the vanilla weather events, instead everything uses VEF
        //WeatherEventControlTab weatherEventControlTab;
        TemperatureControlTab temperatureControlTab;


        public override void DefsLoaded()
        {
            base.DefsLoaded();
            biomeWeatherOverrides.DefsLoaded();
            weatherDurationOverrides.DefsLoaded();
            weatherEventOverrides.DefsLoaded();
        }

        public override void DoWindowContents(Rect inRect)
        {
            SmartSettingsDrawUtility.DrawTabs(inRect, Tabs, tabDrawMode);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look(ref biomeWeatherOverrides, nameof(biomeWeatherOverrides));
            Scribe_Deep.Look(ref weatherDurationOverrides, nameof(weatherDurationOverrides));
            Scribe_Deep.Look(ref weatherEventOverrides, nameof(weatherEventOverrides));
            Scribe_Deep.Look(ref temperatureManager, nameof(temperatureManager));
            if(Scribe.mode == LoadSaveMode.PostLoadInit)
            {
                // added post-release, thus some settings may not have a temperature manager scribed
                if(temperatureManager == null)
                {
                    temperatureManager = new TemperatureControlManager();
                }
            }
        }
    }
}
