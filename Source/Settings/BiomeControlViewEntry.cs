﻿using NightmareCore.Utilities;
using RimWorld;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class BiomeControlViewEntry
    {
        public BiomeDef biome;
        public Dictionary<WeatherDef, float> weatherRatios;

        public BiomeControlViewEntry(BiomeDef biome)
        {
            this.biome = biome;
            weatherRatios = new Dictionary<WeatherDef, float>();
            RefreshRatios();
        }

        public void RefreshRatios()
        {
            weatherRatios.Clear();
            HashSet<WeatherCommonalityRecord> commonalityOverrides = WeatherControlMod.Settings.biomeWeatherOverrides.Get(biome);
            Func<WeatherCommonalityRecord, float> getWeight;
            if(BiomeControlTab.ShouldFactorInWeatherDuration)
            {
                getWeight = (WeatherCommonalityRecord record) => record.weather.durationRange.Average * record.commonality;
            }
            else
            {
                getWeight = (WeatherCommonalityRecord record) => record.commonality;
            }
            float totalWeight = commonalityOverrides.Sum(record => getWeight(record));
            if(totalWeight == 0)
            {
                return;
            }
            foreach(WeatherCommonalityRecord record in commonalityOverrides)
            {
                if(record.commonality == 0)
                {
                    continue;
                }
                float ratio = getWeight(record) / totalWeight;
                weatherRatios.SetOrAdd(record.weather, ratio);
            }
            //Log.Message($"Refreshed Ratios: {string.Join(", ", weatherRatios.Select(kvp => $"{kvp.Key.defName}: {kvp.Value}"))}");
        }
        public float GetRatioOf(WeatherDef weather)
        {
            return weatherRatios.TryGetValue(weather, 0);
        }

        static float RatiosRectHeight => Text.LineHeight * 2;
        const float ratioRectMargin = 4f;
        const float selectedHighlightBorderWidth = 4f;
        const float buttonSizeRatio = 0.6f;
        public void DrawBiome(Listing_Standard list)
        {
            DrawBiomeTopPart(list);
            DrawBiomeBottomPart(list);
        }
        private void DrawBiomeTopPart(Listing_Standard list)
        {
            //list.Label(biome.LabelCap);
            Rect rowRect = list.GetRect(Text.LineHeight);
            float labelWidth = Text.CalcSize(biome.LabelCap).x;
            Rect labelRect = rowRect.LeftPartPixels(labelWidth);
            Widgets.Label(labelRect, biome.LabelCap);
            if(biome == BiomeControlTab.SelectedBiome)
            {
                Rect previousLevelRect = new Rect(labelRect.xMax, labelRect.yMin, rowRect.height, rowRect.height);
                Rect nextLevelRect = new Rect(previousLevelRect.xMax, previousLevelRect.yMin, previousLevelRect.width, previousLevelRect.height);
                previousLevelRect = previousLevelRect.ContractedBy(4f);
                nextLevelRect = nextLevelRect.ContractedBy(4f);
                TooltipHandler.TipRegion(previousLevelRect, "NCWC_Settings_SelectPreviousWeather".Translate());
                TooltipHandler.TipRegion(nextLevelRect, "NCWC_Settings_SelectNextWeather".Translate());
                if (Widgets.ButtonImage(previousLevelRect, UITextures.ArrowLeft))
                {
                    SelectNextWeather(forward: false);
                }
                if (Widgets.ButtonImage(nextLevelRect, UITextures.ArrowRight))
                {
                    SelectNextWeather(forward: true);
                }
            }
        }

        private void SelectNextWeather(bool forward = true)
        {
            int currentIndex = weatherRatios.Keys.FirstIndexOf(w => w == BiomeControlTab.SelectedWeather);
            if(currentIndex == -1)
            {
                return;
            }
            if(forward && currentIndex == weatherRatios.Count - 1)
            {
                currentIndex = 0;
            }
            else if(!forward && currentIndex == 0)
            {
                currentIndex = weatherRatios.Count - 1;
            }
            else
            {
                if (forward)
                {
                    currentIndex++;
                }
                else
                {
                    currentIndex--;
                }
            }
            BiomeControlTab.SelectedWeather = weatherRatios.Keys.ElementAt(currentIndex);
        }

        private void DrawBiomeBottomPart(Listing_Standard list)
        {
            Rect entryRect = list.GetRect(RatiosRectHeight);
            Rect resetRect = entryRect.LeftPartPixels(entryRect.height);
            Rect addRect = entryRect.RightPartPixels(entryRect.height);
            Rect ratiosRect = new Rect(resetRect.xMax, entryRect.yMin, addRect.xMin - resetRect.xMax, entryRect.height);

            DrawResetButton(resetRect);

            Action addAction = () =>
            {
                OpenAddWeatherMenu(biome);
            };
            DrawInteractible(addRect, "", addAction, "NCWC_Settings_AddWeather".Translate());
            UIUtility.TextureInCenter(addRect, TexButton.Plus, out _, addRect.width * buttonSizeRatio, addRect.height * buttonSizeRatio);

            float curX = ratiosRect.xMin;
            float totalWidth = ratiosRect.width - ((weatherRatios.Count - 1) * ratioRectMargin);
            foreach (KeyValuePair<WeatherDef, float> kvp in weatherRatios)
            {
                WeatherDef weather = kvp.Key;
                float ratio = kvp.Value;
                Color color = WeatherColorUtility.AssociatedColorFor(weather);
                float width = totalWidth * ratio;
                Rect weatherRect = new Rect(curX, ratiosRect.yMin, width, RatiosRectHeight);
                curX += width + ratioRectMargin;

                string label = $"{weather.LabelCap}\n{ratio.ToStringPercent()}";
                string tooltip = label;
                if (Prefs.DevMode)
                {
                    tooltip += $"\n{color.r}, {color.g}, {color.b}";
                }
                TooltipHandler.TipRegion(weatherRect, tooltip);
                Action selectedAction = () =>
                {
                    SelectEntry(biome, weather);
                };
                DrawInteractible(weatherRect, label, selectedAction, tooltip, color, GameFont.Small, weather);
            }
        }

        private void DrawResetButton(Rect inRect)
        {
            bool canReset = WeatherControlMod.Settings.biomeWeatherOverrides.modifiedBiomes.Contains(biome);
            Color? iconColor = canReset ? null : (Color?)Color.gray;
            Action resetAction = () =>
            {
                WeatherControlMod.Settings.biomeWeatherOverrides.Reset(biome);
                RefreshRatios();
            };

            DrawInteractible(inRect, "", resetAction, "NCWC_Settings_ResetWeather".Translate());
            UIUtility.TextureInCenter(inRect, UITextures.ResetTexture, out _, inRect.width * buttonSizeRatio, inRect.height * buttonSizeRatio, color: iconColor);
        }

        private void OpenAddWeatherMenu(BiomeDef biome)
        {
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            for(int i = 0; i < DefDatabase<WeatherDef>.AllDefsListForReading.Count; i++)
            {
                WeatherDef weather = DefDatabase<WeatherDef>.AllDefsListForReading[i];
                if(!weatherRatios.ContainsKey(weather))
                {
                    options.Add(new FloatMenuOption(weather.LabelCap, () =>
                    {
                        WeatherControlMod.Settings.biomeWeatherOverrides.Set(biome, weather, 1);
                        //Log.Message($"Added override for biome {biome.defName}, {weather.defName} with value 1");
                        RefreshRatios();
                        SelectEntry(biome, weather);
                    }));
                }
            }
            if(options.NullOrEmpty())
            {
                FloatMenuOption emptyOption = new FloatMenuOption("NCWC_Settings_NoWeatherAvailable".Translate(), null);
                options.Add(emptyOption);
            }
            FloatMenu optionsMenu = new FloatMenu(options);
            Find.WindowStack.Add(optionsMenu);
        }

        private void DrawInteractible(Rect inRect, string label, Action clickAction, string tooltip = null, Color? backgroundColor = null, GameFont? font = null, WeatherDef forWeather = null)
        {
            if(backgroundColor == null)
            {
                backgroundColor = Widgets.WindowBGFillColor;
            }
            if(tooltip != null)
            {
                TooltipHandler.TipRegion(inRect, tooltip);
            }
            bool drawHighlight = Mouse.IsOver(inRect) || (BiomeControlTab.SelectedWeather == forWeather && BiomeControlTab.SelectedBiome == biome);
            if(drawHighlight)
            {
                Widgets.DrawRectFast(inRect, Color.white);
                inRect = inRect.ContractedBy(selectedHighlightBorderWidth);
            }
            Widgets.DrawRectFast(inRect, backgroundColor.Value);
            if(drawHighlight)
            {
                inRect = inRect.ExpandedBy(selectedHighlightBorderWidth);
            }
            Color previousColor = GUI.color;
            GUI.color = WeatherColorUtility.LabelColorForBackgroundColor(backgroundColor.Value);
            String[] labels = label.Split('\n');
            List<Rect> labelRects = UIUtility.SliceRect(inRect, false, labels.Count(), out _, 0);
            for(int i = 0; i < labels.Length; i++)
            {
                Rect labelRect = labelRects[i];
                string lineLabel = DownsizeLabel(labelRect, labels[i]);
                UIUtility.LabelInCenter(labelRect, lineLabel, out _, font: font);
            }
            GUI.color = previousColor;
            if(Widgets.ButtonInvisible(inRect))
            {
                clickAction.Invoke();
            }

            string DownsizeLabel(Rect labelRect, string labelToFormat)
            {
                float requiredWidth = Text.CalcSize(labelToFormat).x;
                if(requiredWidth > labelRect.width)
                {
                    char firstChar = labelToFormat.First();
                    if(Char.IsNumber(firstChar))
                    {
                        return "~";
                    }
                    else
                    {
                        return firstChar.ToString();
                    }
                }
                return labelToFormat;
            }
        }

        public void SelectEntry(BiomeDef biome, WeatherDef weather)
        {
            BiomeControlTab.SelectedBiome = biome;
            BiomeControlTab.SelectedWeather = weather;
        }
    }
}
