﻿using NightmareCore;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{

    public class WeatherDurationControlTab : SmartModSettingsTab
    {
        float contentHeight = 9999f;
        Vector2 scrollPosition;
        static WeatherDurationOverrideManager manager;

        protected override string TabTranslationKey => "NCWC_Settings_WeatherControlTab";

        public WeatherDurationControlTab() : base() { }

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            manager = WeatherControlMod.Settings.weatherDurationOverrides;
        }

        public override void Notify_TabClosed()
        {
            base.Notify_TabClosed();
            manager.ApplyDefModifications();
        }

        public override void DrawContent(Rect inRect)
        {
            UIUtility.MakeAndBeginScrollView(inRect, contentHeight, ref scrollPosition, out Listing_Standard list);
            UIUtility.HeaderLabel(list, "NCWC_Settings_DurationHeader".Translate());

            for(int i = 0; i < DefDatabase<WeatherDef>.AllDefsListForReading.Count; i++)
            {
                if(i != 0)
                {
                    list.GapLine();
                }
                WeatherDef weather = DefDatabase<WeatherDef>.AllDefsListForReading[i];
                IntRange activeRange = WeatherControlMod.Settings.weatherDurationOverrides.Get(weather);
                string durationLabel = $"{activeRange.min.ToStringTicksToPeriodVerbose()} - {activeRange.max.ToStringTicksToPeriodVerbose()}";
                Rect rowRect = list.GetRect(Text.LineHeight);
                List<Rect> slicedRects = UIUtility.SliceRect(rowRect, true, 3, out _);
                Widgets.Label(slicedRects[0], weather.LabelCap);
                TextAnchor previousAnchor = Text.Anchor;
                Text.Anchor = TextAnchor.MiddleCenter;
                Widgets.Label(rowRect, durationLabel);
                Text.Anchor = previousAnchor;

                IntRange newRange = activeRange;
                list.IntRange(ref newRange, Common.MinWeatherDuration, Common.MaxWeatherDuration);
                if(newRange != activeRange)
                {
                    WeatherControlMod.Settings.weatherDurationOverrides.Set(weather, newRange);
                }

                // control buttons need to be called after the range slider, otherwise the slider force-sets the range value
                DoControlButtons(slicedRects[2], weather);
            }

            UIUtility.EndScrollView(list, out contentHeight);
        }

        private void DoControlButtons(Rect inRect, WeatherDef weather)
        {
            bool canReset = WeatherControlMod.Settings.weatherDurationOverrides.modifiedWeathers.Contains(weather);
            Rect resetButtonRect = inRect.RightPartPixels(inRect.height);
            Color iconColor = canReset ? Color.white : Color.gray;
            if(Widgets.ButtonImage(resetButtonRect, UITextures.ResetTexture, iconColor))
            {
                WeatherControlMod.Settings.weatherDurationOverrides.Reset(weather);
            }
        }
    }
    
}
