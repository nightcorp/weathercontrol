﻿using NightmareCore;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace WeatherControl
{

    public class BiomeControlTab : SmartModSettingsTab
    {
        float contentHeight = 9999f;
        Vector2 scrollPosition;
        public BiomeControlTab() : base() { }
        public static BiomeDef SelectedBiome = null;
        public static WeatherDef SelectedWeather = null;
        public static bool ShouldFactorInWeatherDuration = false;
        static BiomeWeatherOverrideManager manager;

        private List<BiomeControlViewEntry> entries = new List<BiomeControlViewEntry>();

        protected override string TabTranslationKey => "NCWC_Settings_BiomeControlTab";

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            entries.Clear();
            foreach(BiomeDef biome in DefDatabase<BiomeDef>.AllDefsListForReading)
            {
                entries.Add(new BiomeControlViewEntry(biome));
            }
            SelectedBiome = entries.First().biome;
            SelectedWeather = entries.First().weatherRatios.First().Key;
            manager = WeatherControlMod.Settings.biomeWeatherOverrides;
        }

        override public void Notify_TabOpened()
        {
            foreach(BiomeControlViewEntry entry in entries)
            {
                entry.RefreshRatios();
            }
        }

        public override void Notify_TabClosed()
        {
            base.Notify_TabClosed();
            manager.ApplyDefModifications();
        }

        public override void DrawContent(Rect inRect)
        {
            UIUtility.MakeAndBeginScrollView(inRect, contentHeight, ref scrollPosition, out Listing_Standard list);
            list.HeaderLabel("NCWC_Settings_BiomeWeatherCommonalities".Translate());
            bool previousShouldFactorInWeatherDuration = ShouldFactorInWeatherDuration;
            list.CheckboxLabeled("NCWC_Settings_FactorInAverageWeatherDuration".Translate(), ref ShouldFactorInWeatherDuration, "NCWC_Settings_FactorInAverageWeatherDuration_Tip".Translate());
            if(previousShouldFactorInWeatherDuration != ShouldFactorInWeatherDuration)
            {
                foreach(BiomeControlViewEntry entry in entries)
                {
                    entry.RefreshRatios();
                }
            }
            foreach(BiomeControlViewEntry entry in entries)
            {
                entry.DrawBiome(list);
                if(entry.biome == SelectedBiome)
                {
                    DrawSelecedEntry(list, entry);
                }
                list.GapLine();
            }
            UIUtility.EndScrollView(list, out contentHeight);
        }

        const int minCommonality = 0;
        const int maxCommonality = 100;


        public void DrawSelecedEntry(Listing_Standard list, BiomeControlViewEntry entry)
        {
            float commonality = WeatherControlMod.Settings.biomeWeatherOverrides.Get(SelectedBiome, SelectedWeather).commonality;
            list.Label($"{SelectedWeather.LabelCap} - {Math.Round((double)commonality, 2)} ({entry.GetRatioOf(SelectedWeather).ToStringPercent()})");
            float newCommonality = list.Slider(commonality, minCommonality, maxCommonality);
            if(newCommonality != commonality)
            {
                WeatherControlMod.Settings.biomeWeatherOverrides.Set(SelectedBiome, SelectedWeather, newCommonality);
                entry.RefreshRatios();
            }
        }

    }
}
