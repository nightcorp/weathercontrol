﻿using NightmareCore;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class DebugTab : SmartModSettingsTab
    {
        public DebugTab() : base() { }

        float contentHeight = 9999f;
        Vector2 scrollPosition;

        protected override string TabTranslationKey => "DEBUG";

        public override void DefsLoaded()
        {
            base.DefsLoaded();
        }
        public override void DrawContent(Rect inRect)
        {
            UIUtility.MakeAndBeginScrollView(inRect, contentHeight, ref scrollPosition, out Listing_Standard list);
            var range = WeatherDurationDefaultsTracker.GetDefaultDurationRange(WeatherDefOf.Clear);
            list.Label(range.min.ToStringTicksToPeriod());
            list.Label(range.min.ToStringTicksToPeriodVague());
            list.Label(range.min.ToStringTicksToPeriodVerbose());
            list.Label(range.min.ToStringTicksToDays());
            UIUtility.EndScrollView(list, out contentHeight);
        }
    }
}
