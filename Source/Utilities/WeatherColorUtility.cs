﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Random = System.Random;

namespace WeatherControl
{
    public static class WeatherColorUtility
    {
        private static Dictionary<WeatherDef, Color> associatedColors = new Dictionary<WeatherDef, Color>();

        public static Color AssociatedColorFor(WeatherDef weather)
        {
            if(!associatedColors.ContainsKey(weather))
            {
                associatedColors[weather] = GetAssociatedColorFor(weather);
            }
            return associatedColors[weather];
        }
        private static Color GetAssociatedColorFor(WeatherDef weather)
        {
            WeatherDefExtension_WeatherControl extension = weather.GetModExtension<WeatherDefExtension_WeatherControl>();
            if(extension?.associatedColor != null)
            {
                return extension.associatedColor.Value;
            }

            Random rand = new Random(weather.defName.GetHashCode());
            return new Color((float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble());
        }

        public static List<Color> DebugColors(WeatherDef weather)
        {
            List<Color> colors = new List<Color>
            {
                weather.skyColorsDay.sky,
                weather.skyColorsDay.overlay,
                weather.skyColorsDay.shadow,
                GetAssociatedColorFor(weather)
            };
            return colors;
        }

        const float maxColorValue = 1;
        public static Color NegativeColorFor(Color inColor)
        {
            Color outColor = new Color(maxColorValue - inColor.r, maxColorValue - inColor.g, maxColorValue - inColor.b);
            Log.ErrorOnce($"Color {inColor} produces negative: {outColor}", inColor.GetHashCode());
            return outColor;
        }

        public static Color LabelColorForBackgroundColor(Color backgroundColor)
        {
            // some luminence math I found online https://discussions.unity.com/t/shaders-brightness-of-a-colour/121247
            float luminence = backgroundColor.r * 0.3f + backgroundColor.g * 0.59f + backgroundColor.b * 0.11f;
            if(luminence > 0.5)
            {
                return Color.black;
            }
            return Color.white;
        }
    }
}
