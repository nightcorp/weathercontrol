﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public static class TileTimeUtility
    {
        public static float GetHour(long absTick, int tile)
        {
            float longitude = Find.WorldGrid.LongLatOf(tile).x;
            absTick += GenDate.LocalTicksOffsetFromLongitude(longitude);
            return absTick / (float)GenDate.TicksPerHour % GenDate.HoursPerDay;
        }

        public static float GetDayOfSeason(long absTick, int tile)
        {
            float longitude = Find.WorldGrid.LongLatOf(tile).x;
            absTick += GenDate.LocalTicksOffsetFromLongitude(longitude);
            float dayAsFloat = absTick / (float)GenDate.TicksPerDay % GenDate.DaysPerSeason;
            dayAsFloat += 1;    // need to add 1, because the first day of a month is not the 0th
            return dayAsFloat;
        }

        public static Season GetSeason(long absTick, int tile)
        {
            Vector2 longLat = Find.WorldGrid.LongLatOf(tile);
            Season season = GenDate.Season(absTick, longLat);
            return season;
        }

        public static Quadrum GetQuadrum(long absTick, int tile)
        {
            Vector2 longLat = Find.WorldGrid.LongLatOf(tile);
            Quadrum quadrum = GenDate.Quadrum(absTick, longLat.y);
            return quadrum;
        }
    }
}
