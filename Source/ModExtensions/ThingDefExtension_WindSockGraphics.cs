﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public class ThingDefExtension_WindSockGraphics : DefModExtension
    {
        public List<GraphicData> graphics;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(graphics.NullOrEmpty())
            {
                yield return $"Required field {nameof(graphics)} must be set and not an empty list";
            }
        }
    }
}
