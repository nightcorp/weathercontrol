﻿#if v1_5
using LudeonTK;
#endif
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public static class DebugActions
    {
        [DebugAction("WeatherControl", "Get all weather events")]
        private static void Debug_GetAllWeatherEvents()
        {
            foreach (WeatherDef weather in DefDatabase<WeatherDef>.AllDefsListForReading)
            {
                List<WeatherEventMaker> events = WeatherControlMod.Settings.weatherEventOverrides.Get(weather);
                string message = $"{weather.defName}: ";
                if (events.NullOrEmpty())
                {
                    message += $"NONE";
                }
                else
                {
                    message += $"{string.Join(", ", events.Select(e => e.eventClass))}";
                }
                Log.Message(message);
            }
        }

        [DebugAction("WeatherControl", "Center Camera", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void CenterCamera()
        {
            CameraJumper.TryJump(Find.CurrentMap.Center, Find.CurrentMap);
            Current.Camera.depth = 0.5f;
            GenSpawn.Spawn(ThingMaker.MakeThing(ThingDefOf.Wall, ThingDefOf.WoodLog), Find.CurrentMap.Center, Find.CurrentMap);
        }
    }
}
