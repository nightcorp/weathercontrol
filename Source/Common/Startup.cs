﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony("WeatherControl");
            harmony.PatchAll();
            ExecuteManualPatches(harmony);
        }

        static void ExecuteManualPatches(Harmony harmony)
        {
            //// the beauty of each of these patches is that they can snipe the original code, no matter how it looks,
            ////  each of them is a replacement of a field to instead call the static managers to get the overrides

            //// biome-weather commonalities
            //HarmonyMethod commonalityTranspiler = new HarmonyMethod(typeof(Patch_WeatherDecider), nameof(Patch_WeatherDecider.TranspileToBiomeWeatherRecordsOverride));
            //harmony.Patch(AccessTools.PropertyGetter(typeof(WeatherDecider), nameof(WeatherDecider.WeatherCommonalities)), transpiler: commonalityTranspiler);
            //harmony.Patch(AccessTools.Method(typeof(WeatherDecider), "CurrentWeatherCommonality"), transpiler: commonalityTranspiler);

            //// weather duration
            //HarmonyMethod durationTranspiler = new HarmonyMethod(typeof(Patch_WeatherDecider), nameof(Patch_WeatherDecider.TranspileWeatherDurationToOverride));
            //harmony.Patch(AccessTools.Method(typeof(WeatherDecider), nameof(WeatherDecider.StartInitialWeather)), transpiler: durationTranspiler);
            //harmony.Patch(AccessTools.Method(typeof(WeatherDecider), nameof(WeatherDecider.StartNextWeather)), transpiler: durationTranspiler);

            //// weather events
            //HarmonyMethod weatherEventTranspiler = new HarmonyMethod(typeof(Patch_WeatherWorker), nameof(Patch_WeatherWorker.TranspileWeatherEventsToOverride));
            //harmony.Patch(AccessTools.Method(typeof(WeatherWorker), nameof(WeatherWorker.WeatherTick)), transpiler: weatherEventTranspiler);
        }
    }
}
