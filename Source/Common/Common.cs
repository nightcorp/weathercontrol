﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherControl
{
    public static class Common
    {
        public const int MaxWeatherDuration = 240000;
        public const int MinWeatherDuration = 1;
        const float EventMaxDurationMultiplier = 0.25f;
        public const float MaxWeatherEventDuration = (int)(MaxWeatherDuration * EventMaxDurationMultiplier);
    }
}
