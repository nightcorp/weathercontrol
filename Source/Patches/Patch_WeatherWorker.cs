﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Reflection.Emit;
//using System.Text;
//using System.Threading.Tasks;
//using HarmonyLib;
//using Verse;

//namespace WeatherControl
//{
//    public static class Patch_WeatherWorker
//    {
//        static FieldInfo eventMakersFI = AccessTools.Field(typeof(WeatherDef), nameof(WeatherDef.eventMakers));
//        static MethodInfo helperGetOverriddenEventsMI = AccessTools.Method(typeof(Patch_WeatherWorker), nameof(HelperMethod_GetOverriddenWeatherEvents));

//        public static IEnumerable<CodeInstruction> TranspileWeatherEventsToOverride(IEnumerable<CodeInstruction> instructions)
//        {
//            foreach (CodeInstruction instruction in instructions)
//            {
//                if(instruction.LoadsField(eventMakersFI))
//                {
//                    foreach(CodeInstruction injectedInstruction in InjectedInstructions())
//                    {
//                        yield return injectedInstruction;
//                    }
//                }
//                else
//                {
//                    yield return instruction;
//                }
//            }

//            IEnumerable<CodeInstruction> InjectedInstructions()
//            {
//                yield return new CodeInstruction(OpCodes.Call, helperGetOverriddenEventsMI);
//            }
//        }

//        static List<WeatherEventMaker> HelperMethod_GetOverriddenWeatherEvents(WeatherDef weather)
//        {
//            return WeatherControlMod.Settings.weatherEventOverrides.Get(weather);
//        }
//    }
//}
