﻿using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Noise;

namespace WeatherControl.Patches
{
    [HarmonyPatch]
    public static class Patch_Temperature
    {
        [HarmonyPatch(typeof(GenTemperature), nameof(GenTemperature.OffsetFromSunCycle))]
        [HarmonyPostfix]
        public static void AddDayTimeOffsetFromTemperatureControl(ref float __result, int absTick, int tile)
        {
            try
            {
                TemperatureControlManager temperatureManager = WeatherControlMod.Settings.temperatureManager;
                if (!temperatureManager.isEnabled)
                {
                    return;
                }
                __result += temperatureManager.GetOffsetAtHour(absTick, tile);
            }
            catch (Exception ex)
            {
                Log.Error($"WeatherControl caught an exception, please report these details: {ex}");
            }
        }

        [HarmonyPatch(typeof(GenTemperature), nameof(GenTemperature.OffsetFromSeasonCycle))]
        [HarmonyPostfix]
        public static void AddSeasonOffsetFromTemperatureControl(ref float __result, int absTick, int tile)
        {
            try
            {
                TemperatureControlManager temperatureManager = WeatherControlMod.Settings.temperatureManager;
                if (!temperatureManager.isEnabled)
                {
                    return;
                }
                __result += temperatureManager.GetOffsetAtDay(absTick, tile);
            }
            catch (Exception ex)
            {
                Log.Error($"WeatherControl caught an exception, please report these details: {ex}");
            }
        }
    }
}
