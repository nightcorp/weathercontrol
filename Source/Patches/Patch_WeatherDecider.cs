﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Reflection.Emit;
//using System.Text;
//using System.Threading.Tasks;
//using HarmonyLib;
//using RimWorld;
//using Verse;

//namespace WeatherControl
//{
//    // patched from Startup
//    public static class Patch_WeatherDecider
//    {
//        #region Weather commonalities
//        static MethodInfo commonalityHelperMI = AccessTools.Method(typeof(Patch_WeatherDecider), nameof(HelperMethod_GetWeatherCommonality));
//        static FieldInfo baseWeatherCommonalitiesFI = AccessTools.Field(typeof(BiomeDef), nameof(BiomeDef.baseWeatherCommonalities));

//        static List<WeatherCommonalityRecord> HelperMethod_GetWeatherCommonality(BiomeDef biome)
//        {
//            return WeatherControlMod.Settings.biomeWeatherOverrides.Get(biome).ToList();
//        }

//        public static IEnumerable<CodeInstruction> TranspileToBiomeWeatherRecordsOverride(IEnumerable<CodeInstruction> instructions)
//        {
//            foreach(CodeInstruction instruction in instructions)
//            {
//                if(instruction.LoadsField(baseWeatherCommonalitiesFI))
//                {
//                    foreach(CodeInstruction injectedInstruction in InjectedInstructions())
//                    {
//                        yield return injectedInstruction;
//                    }
//                }
//                else
//                {
//                    yield return instruction;
//                }
//            }

//            IEnumerable<CodeInstruction> InjectedInstructions()
//            {
//                yield return new CodeInstruction(OpCodes.Call, commonalityHelperMI);
//            }

//        }
//        #endregion

//        #region Weather Duration
//        static MethodInfo durationHelperMI = AccessTools.Method(typeof(Patch_WeatherDecider), nameof(HelperMethod_GetWeatherDurationRange));
//        static FieldInfo weatherDurationFI = AccessTools.Field(typeof(WeatherDef), nameof(WeatherDef.durationRange));
//        static IntRange HelperMethod_GetWeatherDurationRange(WeatherDef weather)
//        {
//            return WeatherControlMod.Settings.weatherDurationOverrides.Get(weather);
//        }

//        public static IEnumerable<CodeInstruction> TranspileWeatherDurationToOverride(IEnumerable<CodeInstruction> instructions)
//        {
//            foreach(CodeInstruction instruction in instructions)
//            {
//                if(instruction.LoadsField(weatherDurationFI))
//                {
//                    foreach(CodeInstruction injectedInstruction in InjectedInstructions())
//                    {
//                        yield return injectedInstruction;
//                    }
//                }
//                else
//                {
//                    yield return instruction;
//                }
//            }
//            IEnumerable<CodeInstruction> InjectedInstructions()
//            {
//                yield return new CodeInstruction(OpCodes.Call, durationHelperMI);
//            }
//        }

//        #endregion
//    }
//}
