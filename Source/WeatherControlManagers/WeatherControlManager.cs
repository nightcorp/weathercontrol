﻿using Mono.Security.Authenticode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public abstract class WeatherControlManager : IExposable
    {
        /// <summary>
        /// Key is the Def (weather or biome), value the hash of the given Def.
        /// These are then used to recognize changes within the Defs between game launches and prompt user action to resolve potential conflicts.
        /// </summary>
        protected Dictionary<string, int> rememberedDefHashes;
        private Dictionary<string, int> currentDefHashes;
        public abstract void ApplyDefModifications();
        public virtual void DefsLoaded()
        {
            CalculateDefHashDifferences();
        }
        private void CalculateDefHashDifferences()
        {
            currentDefHashes = CalculateDefHashes();
            //Log.Message($"Calculated hashes on load: {string.Join(", ", currentDefHashes)}");
            if(rememberedDefHashes == null)
            {
                rememberedDefHashes = currentDefHashes;
                //Log.Message($"Setting initial hashes for {this.GetType()}");
                WeatherControlMod.Settings.Write();
                return;
            }
            List<string> persistingDefs = currentDefHashes.Keys.Intersect(rememberedDefHashes.Keys).ToList();
            List<string> changedDefs = new List<string>();
            foreach (string def in persistingDefs)
            {
                int rememberedHash = rememberedDefHashes[def];
                int currentHash = currentDefHashes[def];
                if(currentHash != rememberedHash)
                {
                    //Log.Message($"Remembered hash for {def} ({rememberedHash}) differs from new one {currentHash}");
                    changedDefs.Add(def);
                }
            }
            if (changedDefs.Any())
            {
                RecoverFromHashDifference(changedDefs);
            }
        }

        public abstract string PresentableLabel { get; }
        public abstract Dictionary<string, int> CalculateDefHashes();
        public void RecoverFromHashDifference(List<string> changedDefs)
        {
            new Window_DefsChangedUserPrompt(this, changedDefs);
        }
        public void Notify_DefChangesResolved()
        {
            rememberedDefHashes = currentDefHashes;
            //Log.Message($"Calculated hashes on resolution: {string.Join(", ", rememberedDefHashes)}");
            WeatherControlMod.Settings.Write();
            ApplyDefModifications();
        }

        /// <summary>
        /// Optionally handles overrides where defs were changed and the user has chosen to remove the overrides in <see cref="Window_DefsChangedUserPrompt"/>
        /// </summary>
        /// <param name="defName"></param>
        public abstract void RemoveOverride(string defName);

        public virtual void ExposeData()
        {
            Scribe_Collections.Look(ref rememberedDefHashes, nameof(rememberedDefHashes), LookMode.Value);
            //Log.Message($"Exposing remembered def hashes, values: {(rememberedDefHashes == null ? "NULL" : string.Join(", ", rememberedDefHashes))}");
        }
    }
}
