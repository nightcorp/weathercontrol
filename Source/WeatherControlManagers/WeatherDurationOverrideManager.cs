﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public class WeatherDurationOverrideManager : WeatherControlManager
    {
        Dictionary<WeatherDef, IntRange> weatherDurationOverrides = new Dictionary<WeatherDef, IntRange>();
        Dictionary<string, IntRange> scribedWeatherDurationOverrides = new Dictionary<string, IntRange>();
        public List<WeatherDef> modifiedWeathers = new List<WeatherDef>();
        List<string> scribedModifiedWeathers = new List<string>();
        public WeatherDurationOverrideManager() : base() { }

        public IntRange Get(WeatherDef weather)
        {
            if(weatherDurationOverrides.TryGetValue(weather, out IntRange durationRange))
            {
                return durationRange;
            }
            durationRange = WeatherDurationDefaultsTracker.GetDefaultDurationRange(weather);
            weatherDurationOverrides.Add(weather, durationRange);
            return durationRange;
        }

        public void Set(WeatherDef weather, IntRange range)
        {
            //Log.Message($"Setting range");
            weatherDurationOverrides.SetOrAdd(weather, range);
            modifiedWeathers.AddDistinct(weather);
        }

        public void Reset(WeatherDef weather)
        {
            weatherDurationOverrides.SetOrAdd(weather, WeatherDurationDefaultsTracker.GetDefaultDurationRange(weather));
            if (modifiedWeathers.Contains(weather))
            {
                modifiedWeathers.Remove(weather);
            }
        }

        public override string PresentableLabel => "NCWC_ManagerName_WeatherDurationOverrides".Translate();

        public override Dictionary<string, int> CalculateDefHashes()
        {
            Dictionary<string, int> hashes = new Dictionary<string, int>();
            foreach (WeatherDef weather in DefDatabase<WeatherDef>.AllDefsListForReading)
            {
                int hash = 0;
                hash += weather.durationRange.GetHashCode();
                hashes[weather.defName] = hash;
            }
            return hashes;
        }

        override public void ApplyDefModifications()
        {
            foreach(KeyValuePair<WeatherDef, IntRange> kvp in weatherDurationOverrides)
            {
                kvp.Key.durationRange = kvp.Value;
            }
        }

        public override void RemoveOverride(string defName)
        {
            WeatherDef def = DefDatabase<WeatherDef>.GetNamed(defName);
            Reset(def);
        }

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            weatherDurationOverrides = new Dictionary<WeatherDef, IntRange>();
            foreach (KeyValuePair<string, IntRange> scribedOverride in scribedWeatherDurationOverrides.ToList())
            {
                string defName = scribedOverride.Key;
                IntRange entry = scribedOverride.Value;
                WeatherDef def = DefDatabase<WeatherDef>.GetNamed(defName, false);
                if (def == null)
                {
                    Log.Warning($"Underlying def {defName} has been removed. Weather duration overrides for this weather have been removed.");
                    scribedWeatherDurationOverrides.Remove(defName);
                }
                else
                {
                    weatherDurationOverrides[def] = entry;
                }
            }
            modifiedWeathers = new List<WeatherDef>();
            foreach (string defName in scribedModifiedWeathers)
            {
                WeatherDef weather = DefDatabase<WeatherDef>.GetNamed(defName, false);
                if(weather != null)
                {
                    modifiedWeathers.Add(weather);
                }
            }
            ApplyDefModifications();
        }

        override public void ExposeData()
        {
            base.ExposeData();
            if (weatherDurationOverrides != null && Scribe.mode == LoadSaveMode.Saving)
            {
                scribedWeatherDurationOverrides = weatherDurationOverrides.ToDictionary(kvp => kvp.Key.defName, kvp => kvp.Value);
            }
            Scribe_Collections.Look(ref scribedWeatherDurationOverrides, nameof(scribedWeatherDurationOverrides), LookMode.Value, LookMode.Value);

            scribedModifiedWeathers = modifiedWeathers.Select(w => w.defName).ToList();
            Scribe_Collections.Look(ref scribedModifiedWeathers, nameof(scribedModifiedWeathers), LookMode.Value);

            if(Scribe.mode == LoadSaveMode.Saving)
            {
                ApplyDefModifications();
            }
        }
    }
}
