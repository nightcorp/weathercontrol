﻿using NightmareCore.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    enum DefChangedResolutionMode
    {
        RemoveOverride,
        IgnoreDef
    }

    public class Window_DefsChangedUserPrompt : Window
    {
        static List<DefChangedResolutionMode> defChangedResolutionModes = new List<DefChangedResolutionMode>()
        {
            DefChangedResolutionMode.RemoveOverride,
            DefChangedResolutionMode.IgnoreDef
        };

        Dictionary<string, DefChangedResolutionMode> changedDefs = new Dictionary<string, DefChangedResolutionMode>();
        WeatherControlManager manager;
        bool isInChoiceMode = false;
        bool isExplanationHidden = false;
        float explanationHeight = float.MaxValue;

        float scrollHeight = float.MaxValue;
        Vector2 scrollPosition = Vector2.zero;
        public Window_DefsChangedUserPrompt(WeatherControlManager manager, List<string> changedDefs)
        {
            this.manager = manager;
            this.changedDefs = changedDefs.ToDictionary(defName => defName, defName => DefChangedResolutionMode.RemoveOverride);

            base.absorbInputAroundWindow = true;
            base.onlyOneOfTypeAllowed = false;

            Find.WindowStack.Add(this);
        }

        public override Vector2 InitialSize => new Vector2(UI.screenWidth * 0.4f, UI.screenHeight * 0.5f);

        public override void DoWindowContents(Rect inRect)
        {
            Rect controlButtonsRect = inRect.BottomPartPixels(Text.LineHeight * 2);
            DrawControls(controlButtonsRect);
            inRect.yMax -= controlButtonsRect.height;

            UIUtility.MakeAndBeginScrollView(inRect, scrollHeight, ref scrollPosition, out Listing_Standard list);
            list.HeaderLabel("NCWC_DefConflict_Header".Translate(manager.PresentableLabel.Named("MANAGERLABEL")));
            Rect explanationRect = UIUtility.CollapsibleRect(list, ref isExplanationHidden, explanationHeight);
            Widgets.Label(explanationRect, "NCWC_DefConflict_Explanation".Translate());
            explanationHeight = Text.CalcHeight("NCWC_DefConflict_Explanation".Translate(), explanationRect.width);

            list.IndentAndReduceWidth();
            foreach (string defName in changedDefs.Keys)
            {
                if (isInChoiceMode)
                {
                    Action<DefChangedResolutionMode> selectionAction = (DefChangedResolutionMode mode) => changedDefs[defName] = mode;
                    UIUtility.DropDownLabeled<DefChangedResolutionMode>(list, defName, changedDefs[defName], defChangedResolutionModes, selectionAction);
                }
                else
                {
                    list.Label(defName);
                }
            }
            list.OutdentAndIncreaseWidth();

            UIUtility.EndScrollView(list, out scrollHeight);
        }

        private void DrawControls(Rect inRect)
        {
            if (isInChoiceMode)
            {
                List<Rect> rects = UIUtility.SliceRect(inRect, true, 2, out _);
                if (Widgets.ButtonText(rects[0], "NCWC_DefConflict_CancelChoices".Translate()))
                {
                    isInChoiceMode = false;
                }
                if (Widgets.ButtonText(rects[1], "NCWC_DefConflict_ConfirmChoices".Translate()))
                {
                    foreach (KeyValuePair<string, DefChangedResolutionMode> changedDef in changedDefs)
                    {
                        if(changedDef.Value == DefChangedResolutionMode.RemoveOverride)
                        {
                            manager.RemoveOverride(changedDef.Key);
                        }
                    }
                    Notify_DefChangesResolved();
                }
            }
            else
            {
                List<Rect> rects = UIUtility.SliceRect(inRect, true, 3, out _);
                TooltipHandler.TipRegion(rects[0], "NCWC_DefConflict_RemoveAllOverrides_Tip".Translate());
                if (Widgets.ButtonText(rects[0], "NCWC_DefConflict_RemoveAllOverrides".Translate()))
                {
                    foreach (string defName in changedDefs.Keys)
                    {
                        manager.RemoveOverride(defName);
                    }
                    Notify_DefChangesResolved();
                }
                TooltipHandler.TipRegion(rects[1], "NCWC_DefConflict_IgnoreAllDefChanges_Tip".Translate());
                if (Widgets.ButtonText(rects[1], "NCWC_DefConflict_IgnoreAllDefChanges".Translate()))
                {
                    Notify_DefChangesResolved();
                }
                TooltipHandler.TipRegion(rects[2], "NCWC_DefConflict_ChooseForEach_Tip".Translate());
                if (Widgets.ButtonText(rects[2], "NCWC_DefConflict_ChooseForEach".Translate()))
                {
                    isInChoiceMode = true;
                }
            }
        }

        private void Notify_DefChangesResolved()
        {
            manager.Notify_DefChangesResolved();
            Close();
        }
    }
}
