﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public class BiomeWeatherOverrideEntry : IExposable
    {
        public HashSet<WeatherCommonalityRecord> Records = new HashSet<WeatherCommonalityRecord>();
        Dictionary<string, float> scribedRecords = new Dictionary<string, float>();

        public BiomeWeatherOverrideEntry() { }
        public BiomeWeatherOverrideEntry(HashSet<WeatherCommonalityRecord> records)
        {
            this.Records = records;
        }

        public void DefsLoaded()
        {
            Records = new HashSet<WeatherCommonalityRecord>();
            foreach (KeyValuePair<string, float> kvp in scribedRecords.ToList())
            {
                WeatherDef weatherDef = DefDatabase<WeatherDef>.GetNamed(kvp.Key, false);
                if(weatherDef == null)
                {
                    Log.Warning($"Underlying weather def {kvp.Key} has been removed. Biomes with this weatehr have had their overrides removed.");
                    scribedRecords.Remove(kvp.Key);
                }
                else
                {
                    WeatherCommonalityRecord record = new WeatherCommonalityRecord()
                    {
                        weather = weatherDef,
                        commonality = kvp.Value
                    };
                    Records.Add(record);
                }
            }
        }

        public void ExposeData()
        {
            if (Records != null)
            {
                foreach (WeatherCommonalityRecord record in Records)
                {
                    scribedRecords[record.weather.defName] = record.commonality;
                }
            }
            Scribe_Collections.Look(ref scribedRecords, nameof(scribedRecords));
        }
    }
}
