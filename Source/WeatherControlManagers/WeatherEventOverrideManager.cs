﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    

    public class WeatherEventOverrideManager : WeatherControlManager
    {
        Dictionary<WeatherDef, WeatherEventOverrideEntry> weatherEventOverrides = new Dictionary<WeatherDef, WeatherEventOverrideEntry>();
        Dictionary<string, WeatherEventOverrideEntry> scribedWeatherEventOverrides = new Dictionary<string, WeatherEventOverrideEntry>();

        public WeatherEventOverrideManager() : base() { }

        public List<WeatherEventMaker> Get(WeatherDef weather)
        {
            List<WeatherEventMaker> entry = weatherEventOverrides.TryGetValue(weather)?.EventMakers;
            if(entry == null)
            {
                entry = new List<WeatherEventMaker>();
                foreach(WeatherEventMaker eventMaker in weather.eventMakers)
                {
                    entry.Add(new WeatherEventMaker()
                    {
                        averageInterval = eventMaker.averageInterval,
                        eventClass = eventMaker.eventClass
                    });
                }
                weatherEventOverrides.Add(weather, new WeatherEventOverrideEntry(entry));
            }
            return entry;
        }

        public void Add(WeatherDef weather, Type weatherEventClass, float averageInterval)
        {
            WeatherEventMaker eventMaker = new WeatherEventMaker()
            {
                averageInterval = averageInterval,
                eventClass = weatherEventClass
            };
            Get(weather).Add(eventMaker);
        }

        public void Remove(WeatherDef weather, WeatherEventMaker maker)
        {
            List<WeatherEventMaker> entries = Get(weather);
            if(entries.Contains(maker))
            {
                entries.Remove(maker);
            }
        }

        public void Reset(WeatherDef weather)
        {
            List<WeatherEventMaker> entries = Get(weather);
            entries.Clear();
            foreach(WeatherEventMaker maker in weather.eventMakers)
            {
                entries.Add(new WeatherEventMaker()
                {
                    averageInterval = maker.averageInterval,
                    eventClass = maker.eventClass
                });
            }
        }

        public override string PresentableLabel => "NCWC_ManagerName_WeatherEvents".Translate();

        public override Dictionary<string, int> CalculateDefHashes()
        {
            Dictionary<string, int> hashes = new Dictionary<string, int>();
            foreach (WeatherDef weather in DefDatabase<WeatherDef>.AllDefsListForReading)
            {
                int hash = 0;
                foreach (WeatherEventMaker eventMaker in weather.eventMakers)
                {
                    hash += eventMaker.eventClass.FullName.GetHashCode();
                    hash += eventMaker.averageInterval.GetHashCode();
                }
                hashes[weather.defName] = hash;
            }
            return hashes;
        }

        public override void ApplyDefModifications()
        {
            foreach(KeyValuePair<WeatherDef, WeatherEventOverrideEntry> kvp in weatherEventOverrides)
            {
                kvp.Key.eventMakers = kvp.Value.EventMakers;
            }
        }

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            foreach (KeyValuePair<string, WeatherEventOverrideEntry> scribedWeatherEventOverride in scribedWeatherEventOverrides.ToList())
            {
                WeatherDef def = DefDatabase<WeatherDef>.GetNamed(scribedWeatherEventOverride.Key, false);

                if (def == null)
                {
                    Log.Warning($"Underlying weather def {scribedWeatherEventOverride.Key} has been removed. Weather event overrides for this biome have been removed.");
                    scribedWeatherEventOverrides.Remove(scribedWeatherEventOverride.Key);
                }
                else
                {
                    weatherEventOverrides[def] = scribedWeatherEventOverride.Value;
                }
            }
            foreach(WeatherEventOverrideEntry eventOverride in weatherEventOverrides.Values)
            {
                eventOverride.DefsLoaded();
            }
        }

        override public void ExposeData()
        {
            base.ExposeData();
            if(Scribe.mode == LoadSaveMode.Saving && weatherEventOverrides != null)
            {
                scribedWeatherEventOverrides = weatherEventOverrides.ToDictionary(kvp => kvp.Key.defName, kvp => kvp.Value);
            }
            Scribe_Collections.Look(ref scribedWeatherEventOverrides, nameof(scribedWeatherEventOverrides), LookMode.Value, LookMode.Deep);
        }

        public override void RemoveOverride(string defName)
        {
            WeatherDef def = DefDatabase<WeatherDef>.GetNamed(defName);
            Reset(def);
        }
    }

    public class ScribableWeatherEventMaker : IExposable
    {
        WeatherEventMaker innerMaker;
        Type scribedEventClass;
        float scribedAverageInterval;

        public Type EventClass => innerMaker.eventClass;
        public float AverageInterval => innerMaker.averageInterval;

        public ScribableWeatherEventMaker(WeatherEventMaker innerMaker)
        {
            this.innerMaker = innerMaker;
        }

        public void ExposeData()
        {
            if(Scribe.mode == LoadSaveMode.Saving)
            {
                scribedEventClass = innerMaker.eventClass;
                scribedAverageInterval = innerMaker.averageInterval;
            }

            Scribe_Values.Look(ref scribedEventClass, nameof(scribedEventClass));
            Scribe_Values.Look(ref scribedAverageInterval, nameof(scribedAverageInterval));

            if(Scribe.mode == LoadSaveMode.LoadingVars)
            {
                innerMaker = new WeatherEventMaker()
                {
                    eventClass = scribedEventClass,
                    averageInterval = scribedAverageInterval
                };
            }
        }
    }
}
