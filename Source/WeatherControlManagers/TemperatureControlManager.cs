﻿#if v1_5
using LudeonTK;
#endif
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public class TemperatureControlManager : IExposable
    {
        public bool isEnabled = false;

        public SimpleCurve timeToTemperatureOffsets;
        private Dictionary<float, float> scribedTimeToTemperatureOffsets;
        public static FloatRange OffsetRange = new FloatRange(-20, 20);
        public Dictionary<Season, SeasonOffsets> seasonOffsets;

        public TemperatureControlManager()
        {
            SetOrResetTimeOffsets();
            seasonOffsets = new Dictionary<Season, SeasonOffsets>()
            {
                { Season.Spring, new SeasonOffsets() },
                { Season.Summer, new SeasonOffsets() },
                { Season.Fall, new SeasonOffsets() },
                { Season.Winter, new SeasonOffsets() },
            };
        }

        public void ExposeData()
        {
            Scribe_Values.Look(ref isEnabled, nameof(isEnabled));
            if (Scribe.mode == LoadSaveMode.Saving)
            {
                scribedTimeToTemperatureOffsets = timeToTemperatureOffsets.ToDictionary(point => point.x, point => point.y);
            }
            Scribe_Collections.Look(ref scribedTimeToTemperatureOffsets, nameof(scribedTimeToTemperatureOffsets), LookMode.Value, LookMode.Value);
            if(Scribe.mode == LoadSaveMode.LoadingVars)
            {
                timeToTemperatureOffsets.SetPoints(scribedTimeToTemperatureOffsets.Select(kvp => new CurvePoint(kvp.Key, kvp.Value)));
            }
            Scribe_Collections.Look(ref seasonOffsets, nameof(seasonOffsets), LookMode.Value, LookMode.Deep);
        }

        public void SetOrResetTimeOffsets()
        {
            timeToTemperatureOffsets = new SimpleCurve();
            for (int i = 0; i < GenDate.HoursPerDay; i++)
            {
                timeToTemperatureOffsets.Add(i, 0);
            }
        }

        public void ResetSeasonoffsets(Season season)
        {
            seasonOffsets[season] = new SeasonOffsets();
        }

        public float GetOffsetAtHour(long absTick, int tile)
        {
            float hour = TileTimeUtility.GetHour(absTick, tile);
            float offset = timeToTemperatureOffsets.Evaluate(hour);
            return offset;
        }

        public float GetOffsetAtDay(long absTick, int tile)
        {
            float day = TileTimeUtility.GetDayOfSeason(absTick, tile);
            Season currentSeason = TileTimeUtility.GetSeason(absTick, tile);
            if (currentSeason == Season.PermanentWinter)
            {
                currentSeason = Season.Winter;
            }
            if (currentSeason == Season.PermanentSummer)
            {
                currentSeason = Season.Summer;
            }
            SeasonOffsets offsets = seasonOffsets.TryGetValue(currentSeason, null);
            if(offsets == null)
            {
                string errorMessage = $"Could not find season {currentSeason}, only seasons available: {string.Join($", ", seasonOffsets.Keys)}";
                Log.ErrorOnce(errorMessage, errorMessage.GetHashCode());
                return 0;
            }
            float offset = offsets.dayToOffsets.Evaluate(day);
            return offset;
        }

        [DebugAction("WeatherControl", "Log current offsets", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void Debug_LogCurrentOffset()
        {
            int tile = Find.CurrentMap.Tile;
            int tick = GenTicks.TicksAbs;
            GetCurrentOffsetData(out float day, out float dayOffset, out float hour, out float timeOffset, out Quadrum quadrum, out Season season);
            Log.Message($"Offset at tile {tile}, tick {tick}, season: {season}, for day {day}, quadrum {quadrum}: {dayOffset}, for time {hour}: {timeOffset}");
        }

        public static void GetCurrentOffsetData(out float day, out float dayOffset, out float hour, out float hourOffset, out Quadrum quadrum, out Season season)
        {
            int tick = GenTicks.TicksAbs;
            int tile = Find.CurrentMap.Tile;
            day = TileTimeUtility.GetDayOfSeason(tick, tile);
            TemperatureControlManager manager = WeatherControlMod.Settings.temperatureManager;
            dayOffset = manager.GetOffsetAtDay(tick, tile);
            season = TileTimeUtility.GetSeason(tick, tile);
            quadrum = TileTimeUtility.GetQuadrum(tick, tile);
            hour = TileTimeUtility.GetHour(tick, tile);
            hourOffset = manager.GetOffsetAtHour(tick, tile);
        }
    }

    public class SeasonOffsets : IExposable
    {
        //public Dictionary<float, float> dayToOffsets;
        public SimpleCurve dayToOffsets;
        private Dictionary<float, float> scribedDayToOffets;

        public SeasonOffsets()
        {
            dayToOffsets = new SimpleCurve();
            // start at 1, so the first entry is 1 and not 0, which makes little sense to the user in regards to "day of season"
            for (int i = 1; i <= GenDate.DaysPerSeason; i += 1)
            {
                dayToOffsets.Add(i, 0);
            }
        }

        public void ExposeData()
        {
            if (Scribe.mode == LoadSaveMode.Saving)
            {
                scribedDayToOffets = dayToOffsets.ToDictionary(point => point.x, point => point.y);
            }
            Scribe_Collections.Look(ref scribedDayToOffets, nameof(scribedDayToOffets), LookMode.Value, LookMode.Value);
            if (Scribe.mode == LoadSaveMode.LoadingVars)
            {
                dayToOffsets.SetPoints(scribedDayToOffets.Select(kvp => new CurvePoint(kvp.Key, kvp.Value)));
            }
        }
    }
}
