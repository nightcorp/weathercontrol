﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace WeatherControl
{
    public class BiomeWeatherOverrideManager : WeatherControlManager
    {
        Dictionary<BiomeDef, BiomeWeatherOverrideEntry> biomeWeatherOverrides = new Dictionary<BiomeDef, BiomeWeatherOverrideEntry>();
        Dictionary<string, BiomeWeatherOverrideEntry> scribedBiomeWeatherOverrides = new Dictionary<string, BiomeWeatherOverrideEntry>();
        public List<BiomeDef> modifiedBiomes = new List<BiomeDef>();
        private List<string> scribedModifiedBiomes = new List<string>();

        public BiomeWeatherOverrideManager() : base() { }

        public HashSet<WeatherCommonalityRecord> Get(BiomeDef biome)
        {
            if(biome == null)
            {
                Log.Error($"Tried to retrieve for NULL biome");
                return new HashSet<WeatherCommonalityRecord>();
            }
            if(!biomeWeatherOverrides.ContainsKey(biome))
            {
                Add(biome);
            }
            HashSet<WeatherCommonalityRecord> records = biomeWeatherOverrides[biome].Records;
            bool noWeatherPossible = records.Count == 0 || records.All(r => r.commonality <= 0);
            if(noWeatherPossible)
            {
                Log.Warning($"No commonalities set for {biome.defName}, forcing Clear weather with commonality 1");
                WeatherCommonalityRecord clearRecord = records.FirstOrDefault(r => r.weather == WeatherDefOf.Clear);
                if(clearRecord == null)
                {
                    clearRecord = new WeatherCommonalityRecord()
                    {
                        weather = WeatherDefOf.Clear,
                        commonality = 1
                    };
                    records.Add(clearRecord);
                }
                clearRecord.commonality = 1;
            }
            return records;
        }

        public WeatherCommonalityRecord Get(BiomeDef biome, WeatherDef weather)
        {
            HashSet<WeatherCommonalityRecord> records = Get(biome);
            WeatherCommonalityRecord existingEntry = records.FirstOrDefault(rec => rec.weather == weather);
            if(existingEntry == null)
            {
                existingEntry = new WeatherCommonalityRecord()
                {
                    weather = weather,
                    commonality = 1
                };
                records.Add(existingEntry);
            }
            return existingEntry;
        }

        private void Add(BiomeDef biome)
        {
            HashSet<WeatherCommonalityRecord> records = new HashSet<WeatherCommonalityRecord>();
            foreach(WeatherCommonalityRecord record in biome.baseWeatherCommonalities)
            {
                // in edge cases where mods are set up with incorrect weather, this may cause a NRE
                if(record?.weather == null)
                {
                    continue;
                }
                records.Add(new WeatherCommonalityRecord()
                {
                    weather = record.weather,
                    commonality = record.commonality
                });
            }
            biomeWeatherOverrides.Add(biome, new BiomeWeatherOverrideEntry(records));
        }

        public void Set(BiomeDef biome, WeatherDef weather, float value)
        {
            Get(biome, weather).commonality = value;
            //Log.Message($"Set {biome}, {weather} to commonality {value}, actual value: {Get(biome, weather).commonality}");
            HashSet<WeatherCommonalityRecord> records = Get(biome);
            bool noCommonalitiesSet = records.Count == 0 || records.All(rec => rec.commonality <= 0);
            if(noCommonalitiesSet)
            {
                Get(biome, WeatherDefOf.Clear).commonality = 1;
                Messages.Message("NCWC_Settings_NoCommonalities".Translate(biome.LabelCap), MessageTypeDefOf.RejectInput);
            }
            modifiedBiomes.AddDistinct(biome);
        }

        public void Reset(BiomeDef biome)
        {
            Get(biome).Clear();
            foreach(KeyValuePair<WeatherDef, float> weatherWithCommonality in BiomeWeatherDefaultsTracker.GetDefaultWeathersWithCommonalities(biome))
            {
                Set(biome, weatherWithCommonality.Key, weatherWithCommonality.Value);
            }
            if(modifiedBiomes.Contains(biome))
            {
                modifiedBiomes.Remove(biome);
            }
            BiomeControlTab.SelectedWeather = null;
            BiomeControlTab.SelectedBiome = null;
        }

        public override string PresentableLabel => "NCWC_ManagerName_BiomeWeatherOverrides".Translate();

        public override Dictionary<string, int> CalculateDefHashes()
        {
            Dictionary<string, int> hashes = new Dictionary<string, int>();
            foreach(BiomeDef biome in DefDatabase<BiomeDef>.AllDefsListForReading)
            {
                int hash = 0;
                foreach(WeatherCommonalityRecord record in biome.baseWeatherCommonalities)
                {
                    hash += record.weather.defName.GetHashCode();
                    hash += record.commonality.GetHashCode();
                }
                hashes[biome.defName] = hash;
            }
            return hashes;
        }

        override public void ApplyDefModifications()
        {
            foreach(KeyValuePair<BiomeDef, BiomeWeatherOverrideEntry> kvp in biomeWeatherOverrides)
            {
                BiomeDef biome = kvp.Key;
                List<WeatherCommonalityRecord> records = kvp.Value.Records.ToList();
                biome.baseWeatherCommonalities = records;
            }
        }

        public override void RemoveOverride(string defName)
        {
            BiomeDef def = DefDatabase<BiomeDef>.GetNamed(defName);
            Reset(def);
        }

        override public void DefsLoaded()
        {
            base.DefsLoaded();
            biomeWeatherOverrides = new Dictionary<BiomeDef, BiomeWeatherOverrideEntry>();
            foreach(KeyValuePair<string, BiomeWeatherOverrideEntry> scribedOverride in scribedBiomeWeatherOverrides)
            {
                string defName = scribedOverride.Key;
                BiomeWeatherOverrideEntry entry = scribedOverride.Value;
                BiomeDef def = DefDatabase<BiomeDef>.GetNamed(defName, false);
                if(def == null)
                {
                    Log.Warning($"Underlying biome def {defName} has been removed. Biome weather overrides for this biome have been removed.");
                }
                else
                {
                    biomeWeatherOverrides[def] = entry;
                }
            }
            foreach(BiomeWeatherOverrideEntry weatherOverride in biomeWeatherOverrides.Values)
            {
                weatherOverride.DefsLoaded();
            }
            modifiedBiomes = new List<BiomeDef>();
            if(scribedModifiedBiomes != null)
            {
                foreach(string defName in scribedModifiedBiomes)
                {
                    BiomeDef biome = DefDatabase<BiomeDef>.GetNamed(defName, false);
                    if(biome != null)
                    {
                        modifiedBiomes.Add(biome);
                    }
                }
            }
            ApplyDefModifications();
        }

        override public void ExposeData()
        {
            base.ExposeData();
            if(biomeWeatherOverrides != null && Scribe.mode == LoadSaveMode.Saving)
            {
                scribedBiomeWeatherOverrides = biomeWeatherOverrides.ToDictionary(kvp => kvp.Key.defName, kvp => kvp.Value);
            }
            Scribe_Collections.Look(ref scribedBiomeWeatherOverrides, nameof(scribedBiomeWeatherOverrides), LookMode.Value, LookMode.Deep);

            if(Scribe.mode == LoadSaveMode.Saving)
            {
                scribedModifiedBiomes = modifiedBiomes.Select(b => b.defName).ToList();
            }
            Scribe_Collections.Look(ref scribedModifiedBiomes, nameof(scribedModifiedBiomes), LookMode.Value);

            if(Scribe.mode == LoadSaveMode.Saving)
            {
                ApplyDefModifications();
            }
        }
    }
}
