﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    public class WeatherEventOverrideEntry : IExposable
    {
        public List<WeatherEventMaker> EventMakers = new List<WeatherEventMaker>();
        Dictionary<Type, float> scribedEventMakers = new Dictionary<Type, float>();

        public WeatherEventOverrideEntry() { }
        public WeatherEventOverrideEntry(List<WeatherEventMaker> eventMakers)
        {
            this.EventMakers = eventMakers;
        }


        public void DefsLoaded()
        {
            EventMakers = scribedEventMakers.Select(kvp => new WeatherEventMaker()
            {
                eventClass = kvp.Key,
                averageInterval = kvp.Value
            }).ToList();
        }

        public void ExposeData()
        {
            if (EventMakers != null && Scribe.mode == LoadSaveMode.Saving)
            {
                scribedEventMakers = EventMakers.ToDictionary(maker => maker.eventClass, maker => maker.averageInterval);
            }
            Scribe_Collections.Look(ref scribedEventMakers, nameof(scribedEventMakers), LookMode.Value, LookMode.Value);
        }
    }
}
