﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WeatherControl
{
    [StaticConstructorOnStartup]
    public static class WeatherDurationDefaultsTracker
    {
        static Dictionary<WeatherDef, IntRange> weatherDefaults;

        static WeatherDurationDefaultsTracker()
        {
            LoadDefaults();
        }

        private static void LoadDefaults()
        {
            weatherDefaults = new Dictionary<WeatherDef, IntRange>();
            foreach(WeatherDef weather in DefDatabase<WeatherDef>.AllDefsListForReading)
            {
                weatherDefaults.Add(weather, weather.durationRange);
            }
        }

        /// <returns><see cref="IntRange.zero"/> when no entry found</returns>
        public static IntRange GetDefaultDurationRange(WeatherDef weather)
        {
            return weatherDefaults.TryGetValue(weather, IntRange.zero);
        }
    }
}
