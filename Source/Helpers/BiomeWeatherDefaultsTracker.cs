﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace WeatherControl
{
    [StaticConstructorOnStartup]
    public static class BiomeWeatherDefaultsTracker
    {
        static Dictionary<BiomeDef, Dictionary<WeatherDef, float>> biomeWeatherDefaults;

        static BiomeWeatherDefaultsTracker()
        {
            biomeWeatherDefaults = new Dictionary<BiomeDef, Dictionary<WeatherDef, float>>();
            LoadWeatherDefaults();
        }

        private static void LoadWeatherDefaults()
        {
            foreach(BiomeDef biome in DefDatabase<BiomeDef>.AllDefsListForReading)
            {
                biomeWeatherDefaults.SetOrAdd(biome, new Dictionary<WeatherDef, float>());
                foreach(WeatherCommonalityRecord record in biome.baseWeatherCommonalities)
                {
                    // in edge cases where mod developers misconfigure their commonalities with broken references, this may produce NULL records
                    if(record?.weather == null)
                    {
                        continue;
                    }
                    biomeWeatherDefaults[biome][record.weather] = record.commonality;
                }
            }
            //Log.Message($"loaded weather defaults:\n{string.Join("\n", biomeWeatherDefaults.Select(bwd => $"{bwd.Key.defName}: {string.Join(", ", bwd.Value.Select(wd => $"{wd.Key}: {wd.Value}"))}"))}");
        }

        public static Dictionary<WeatherDef, float> GetDefaultWeathersWithCommonalities(BiomeDef biome)
        {
            return biomeWeatherDefaults[biome];
        }

        /// <returns>0 if no default record found</returns>
        public static float GetDefaultCommonality(BiomeDef biome, WeatherDef weather)
        {
            Dictionary<WeatherDef, float> records = biomeWeatherDefaults.TryGetValue(biome, null);
            if(records.NullOrEmpty())
            {
                return 0;
            }
            float commonality = records.TryGetValue(weather, 0);
            //Log.Message($"default commonality for {weather.defName} in biome {biome} is {commonality}");
            return commonality;
        }
    }
}
