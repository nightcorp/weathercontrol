﻿using HarmonyLib;
using RimWorld;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class Building_WeatherStation : Building
    {
        const int refreshRateTicks = 100;
        string _inspectString;

        public override void Tick()
        {
            base.Tick();
            if(GenTicks.TicksGame % refreshRateTicks == 0)
            {
                RecacheInspectString();
            }
        }

        private void RecacheInspectString()
        {
            WeatherManager manager = Map.weatherManager;
            //WeatherDecider decider = Map.weatherDecider;
            List<string> lines = new List<string>();
            _inspectString = "";
            if (manager.RainRate > 0)
            {
                lines.Add($"{"NCWC_WeatherStation_RainRate".Translate(manager.RainRate.ToStringPercent())}");
            }
            if (manager.SnowRate > 0)
            {
                lines.Add($"{"NCWC_WeatherStation_SnowRate".Translate(manager.SnowRate.ToStringPercent())}");
            }
            if (Map.windManager.WindSpeed > 0)
            {
                lines.Add($"{"NCWC_WeatherStation_WindSpeed".Translate(Map.windManager.WindSpeed.ToStringPercent())}");
            }
            if (manager.CurMoveSpeedMultiplier != 1)
            {
                lines.Add($"{"NCWC_WeatherStation_MoveSpeedMultiplier".Translate(manager.CurMoveSpeedMultiplier.ToStringPercent())}");
            }
            if (manager.CurWeatherAccuracyMultiplier != 1)
            {
                lines.Add($"{"NCWC_WeatherStation_AccuracyMultiplier".Translate(manager.CurWeatherAccuracyMultiplier.ToStringPercent())}");
            }
            if (WeatherControlMod.Settings.temperatureManager.isEnabled)
            {
                TemperatureControlManager.GetCurrentOffsetData(out float day, out float dayOffset, out float hour, out float hourOffset, out Quadrum quadrum, out _);
                if (dayOffset != 0)
                {
                    int displayDay = Mathf.RoundToInt(day) + 1;
                    string dayString = $"{quadrum.Label()} {Find.ActiveLanguageWorker.OrdinalNumber(displayDay, Gender.None)}";
                    lines.Add($"NCWC_WeatherStation_TemperatureManager_OffsetDay".Translate(dayString, dayOffset.ToStringTemperatureOffset()));
                }
                if (hourOffset != 0)
                {
                    string hourString = $"{Mathf.RoundToInt(hour) + 1}{"LetterHour".Translate()}";
                    lines.Add($"NCWC_WeatherStation_TemperatureManager_OffsetHour".Translate(hourString, hourOffset.ToStringTemperatureOffset()));
                }
            }
            
            WeatherDef nextWeather = NextGuessedWeather(out float chance);
            lines.Add($"{"NCWC_WeatherStation_NextWeather".Translate(nextWeather.LabelCap, chance.ToStringPercent())}");
            _inspectString = string.Join("\n", lines);
        }

        private WeatherDef NextGuessedWeather(out float chance)
        {
            Dictionary<WeatherDef, float> weatherCommonalities = DefDatabase<WeatherDef>.AllDefsListForReading
                .ToDictionary(w => w, w => CommonalityFor(w));
            float totalCommonality = weatherCommonalities.Sum(kvp => kvp.Value);
            weatherCommonalities.Remove(Map.weatherManager.curWeather);
            KeyValuePair<WeatherDef, float> nextWeather = weatherCommonalities
                .MaxBy(kvp => kvp.Value);
            chance = nextWeather.Value / totalCommonality;
            return nextWeather.Key;
        }

        private static MethodInfo CurrentWeatherCommonality = AccessTools.Method(typeof(WeatherDecider), "CurrentWeatherCommonality");
        private float CommonalityFor(WeatherDef weather)
        {
            return (float)CurrentWeatherCommonality.Invoke(Map.weatherDecider, new object[] { weather });
        }

        public override string GetInspectString()
        {
            string text = base.GetInspectString();

            if(_inspectString == null)
            {
                RecacheInspectString();
            }

            return text + _inspectString;
        }
    }
}
