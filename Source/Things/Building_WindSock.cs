﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace WeatherControl
{
    public class Building_WindSock : Building
    {
		private static readonly FloatRange WindSpeedRange = new FloatRange(0.04f, 2f); // copied from base-game
        static Dictionary<float, Graphic> sockGraphics;
        Graphic _graphic;

        const int refreshRateTicks = 10;
        public override Graphic Graphic
        {
            get
            {
                if (_graphic == null)
                {
                    SetSockGraphic();
                }
                return _graphic;
            }
        }

        public override void Tick()
        {
            base.Tick();
            if(GenTicks.TicksGame % refreshRateTicks == 0)
            {

                SetSockGraphic();
            }
        }

        private void SetSockGraphic()
        {
            if (sockGraphics == null)
            {
                CacheGraphics();
            }
            _graphic = sockGraphics.Last(kvp => Map.windManager.WindSpeed >= kvp.Key).Value;
        }

        private void CacheGraphics()
        {
            sockGraphics = new Dictionary<float, Graphic>();
            ThingDefExtension_WindSockGraphics extension = def.GetModExtension<ThingDefExtension_WindSockGraphics>();
            float thresholdStepSize = (WindSpeedRange.max - WindSpeedRange.min) / (extension.graphics.Count + 1);
            for(int i = 0; i < extension.graphics.Count; i++)
            {
                float threshold = (i + 1) * thresholdStepSize + WindSpeedRange.min;
                if(i == 0)
                {
                    threshold = 0;
                }
                GraphicData graphicData = extension.graphics[i];
                sockGraphics.Add(threshold, graphicData.Graphic);
            }
        }

        public override string GetInspectString()
        {
            string text = base.GetInspectString();
            if(text != "")
            {
                text += "\n";
            }
            text += $"{"NCWC_WindSock_CurrentWindSpeed".Translate(Map.windManager.WindSpeed.ToStringPercent())}";

            return text;
        }
    }
}
