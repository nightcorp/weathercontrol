﻿using HarmonyLib;
using Microsoft.Win32;
using NightmareCore.Utilities;
using RimWorld;
using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Verse;
using Verse.Noise;
using Verse.Sound;

namespace WeatherControl
{
    public class PlaceWorker_MustBeOutdoors : PlaceWorker
    {
        public override AcceptanceReport AllowsPlacing(BuildableDef checkingDef, IntVec3 loc, Rot4 rot, Map map, Thing thingToIgnore = null, Thing thing = null)
        {
            Room room = loc.GetRoom(map);
            if (room != null && !room.UsesOutdoorTemperature)
            {
                return "NCWC_MustBePlacedOutdoors".Translate();
            }
            return AcceptanceReport.WasAccepted;
        }
    }
}
