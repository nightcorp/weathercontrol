The main repo is maintained at https://gitlab.com/nightcorp/realisticmanhunters

# Contributing Bug Reports
If you have found a bug, please check the existing issues and add your details to already existing issues instead of opening new issues!
Should you be the first to report a given bug, create a new Issue and provide the following details:
- HugsLog (https://rentry.co/HowToHugsLog)
- Steps to reproduce your issue
- If useful, screenshots of the issue

Ensure that you have a functional mod load order and game. If you already experience issues with mechanics not related to Archaeology, resolve those before posting your bug reports for Archaeology, it is very difficult to track issues with Archaeology when other mods are also in a broken state and potentially swarming the log.

# Contributing Code
- When providing pull or merge requests, please do not provide the built assemblies, as those will *always* cause a merge conflict.
- You may use tabs or spaces, I genuinely don't care if it's mixed format because VS displays both variants the exact same way.
- Try to follow my style, I am an outspoken enemy of implicit code and behaviour, if you have questions, feel free to contact me directly.
My style is by no means the best, but it helps to keep the project streamlined and from diverging in code style.
  - Encapsulate your code, if your logic can be fragmented into methods and classes, do so
  - Follow the [MicroSoft Coding Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)
  - Use var or new() very sparingly (only if the (generic) type is absurdly long), I'm a big fan of explicit types
  - Prefix any new Base-Game Def with the prefix that other Defs in this mod use - if you are creating a new Def for a Def-Type that this mod implements, you can omit the prefix.
  - Only use ternary operators for variable definitions, and never nest ternary operators
  - Try to use early-return if() conditions for false-y logic instead of calculating a massive bool or returning that directly.
  - Keep if() arguments light, best case you simply pass a bool variable into the if, and have the logic to calculate the bool in the line(s) before.
  - Members are generally arranged in this order: fields, properties, constructors, abstract methods, methods, base-type overrides (ToString, Equals)
    - Always put public before private members, so public fields first, then private fields
	- Notable exceptions are caching List/Dictionary fields that are normally placed above their consuming member.
  - You will find numerous examples of exactly these rules all over the code